# @ztax/tranz

transcription CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)  

- [Usage](#usage)
- [Commands](#commands)

# Usage

# Commands

- [`tranz help [COMMANDS]`](#tranz-help-commands)
- [`tranz plugins`](#tranz-plugins)
- [`tranz plugins:install PLUGIN...`](#tranz-pluginsinstall-plugin)
- [`tranz plugins:inspect PLUGIN...`](#tranz-pluginsinspect-plugin)
- [`tranz plugins:install PLUGIN...`](#tranz-pluginsinstall-plugin-1)
- [`tranz plugins:link PLUGIN`](#tranz-pluginslink-plugin)
- [`tranz plugins:uninstall PLUGIN...`](#tranz-pluginsuninstall-plugin)
- [`tranz plugins:uninstall PLUGIN...`](#tranz-pluginsuninstall-plugin-1)
- [`tranz plugins:uninstall PLUGIN...`](#tranz-pluginsuninstall-plugin-2)
- [`tranz plugins update`](#tranz-plugins-update)
- [`tranz prep INPUT`](#tranz-prep-input)

## `tranz help [COMMANDS]`

Display help for tranz.

```
USAGE
  $ tranz help [COMMANDS] [-n]

ARGUMENTS
  COMMANDS  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for tranz.
```

_See code:_ [_@oclif/plugin-help_](https://github.com/oclif/plugin-help/blob/v5.2.15/src/commands/help.ts)

## `tranz plugins`

List installed plugins.

```
USAGE
  $ tranz plugins [--json] [--core]

FLAGS
  --core  Show core plugins.

GLOBAL FLAGS
  --json  Format output as json.

DESCRIPTION
  List installed plugins.

EXAMPLES
  $ tranz plugins
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/index.ts)

## `tranz plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ tranz plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ tranz plugins add

EXAMPLES
  $ tranz plugins:install myplugin

  $ tranz plugins:install https://github.com/someuser/someplugin

  $ tranz plugins:install someuser/someplugin
```

## `tranz plugins:inspect PLUGIN...`

Displays installation properties of a plugin.

```
USAGE
  $ tranz plugins:inspect PLUGIN...

ARGUMENTS
  PLUGIN  [default: .] Plugin to inspect.

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

GLOBAL FLAGS
  --json  Format output as json.

DESCRIPTION
  Displays installation properties of a plugin.

EXAMPLES
  $ tranz plugins:inspect myplugin
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/inspect.ts)

## `tranz plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ tranz plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ tranz plugins add

EXAMPLES
  $ tranz plugins:install myplugin

  $ tranz plugins:install https://github.com/someuser/someplugin

  $ tranz plugins:install someuser/someplugin
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/install.ts)

## `tranz plugins:link PLUGIN`

Links a plugin into the CLI for development.

```
USAGE
  $ tranz plugins:link PLUGIN

ARGUMENTS
  PATH  [default: .] path to plugin

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Links a plugin into the CLI for development.
  Installation of a linked plugin will override a user-installed or core plugin.

  e.g. If you have a user-installed or core plugin that has a 'hello' command, installing a linked plugin with a 'hello'
  command will override the user-installed or core plugin implementation. This is useful for development work.


EXAMPLES
  $ tranz plugins:link myplugin
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/link.ts)

## `tranz plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ tranz plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ tranz plugins unlink
  $ tranz plugins remove
```

## `tranz plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ tranz plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ tranz plugins unlink
  $ tranz plugins remove
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/uninstall.ts)

## `tranz plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ tranz plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ tranz plugins unlink
  $ tranz plugins remove
```

## `tranz plugins update`

Update installed plugins.

```
USAGE
  $ tranz plugins update [-h] [-v]

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Update installed plugins.
```

_See code:_ [_@oclif/plugin-plugins_](https://github.com/oclif/plugin-plugins/blob/v3.1.8/src/commands/plugins/update.ts)

## `tranz prep INPUT`

Prepare audio file - normalize, noise reduce, split on silence

```
USAGE
  $ tranz prep INPUT [-n] [-o <value>] [-s <value>] [-d <value>]

ARGUMENTS
  INPUT  input file

FLAGS
  -d, --sildur=<value>   [default: 1.1] silence duration
  -n, --norm             do normalization?
  -o, --output=<value>   [default: ./out] output directory
  -s, --silence=<value>  [default: -45dB] silence threshold

DESCRIPTION
  Prepare audio file - normalize, noise reduce, split on silence

EXAMPLES
  $ tranz prep hello friend --from oclif
  hello friend from oclif! (./src/commands/hello/index.ts)
```

_See code:_ [_dist/commands/prep/index.ts_](https://gitlab.com/onezoomin/ztax/tranz/blob/v0.0.1/dist/commands/prep/index.ts)

```
$ npm install -g @ztax/tranz
$ tranz COMMAND
running command...
$ tranz (--version)
@ztax/tranz/0.0.1 linux-x64 node-v18.16.1
$ tranz --help [COMMAND]
USAGE
$ tranz COMMAND
...
```
