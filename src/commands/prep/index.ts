import { Args, Command, Config, Flags, ux } from '@oclif/core'
import ffmpeg from 'fluent-ffmpeg'
import * as fs from 'fs'
import { getFileInfo, getName, getNameWithExt } from '../../utils/file-utils.js'

// @ts-expect-error
import normalize from '@dharmendrasha/ffmpeg-normalize'
import path from 'path'

// Logger import { Logger } from '@ztax/logger/src' //TODO ESM imports

type GenericObject = Record<string, any>
type FfmpegResults = { ffmpegChain: any, outputPath: string }
type SilInfo = { stSec: number, endSec: number, duration: number }
type PrepProps = { input: string, outdir: string, silthr: string, sildur: string, norm: boolean, verbose: boolean }
type PrepResult = { props: PrepProps, info: GenericObject, results: FfmpegResults }


const doSilenceRemoval = async (inputPath: string, silence: string | number, sildur: string | number, output = `${inputPath}-silenceRemoved.mp3`) => {
  ffmpeg(inputPath, { logger: console })
    .audioFilters([
      {
        filter: 'silenceremove',
        options: { stop_threshold: silence, stop_periods: -1, stop_duration: sildur, /* stop_silence: 0.25,  start_silence: 0.25 */ }
      }
    ])
    .on('codecData', function (data: any) {
      // console.log('Input is ' + data.audio + ' audio ' + 'with ' + data.video + ' video');
    })
    .on('stderr', function (stderrLine) {
      console.log('Stderr output: ' + stderrLine)
    })
    .on('error', function (error) {
      console.error('Error:', error, silence);
    })
    .on('end', function (stdout, stderr) {
      console.log('Silence Removal succeeded,  output path:', output);
    })
    .save(output)
}

const doNormalization = async (path: string, outputDir: string) => {
  // const [name, ext] = getExt(path) as string[]
  const { name, ext } = getFileInfo(path)

  const output = `${outputDir}/${name}_normalized${ext}`
  const normalized: any = await normalize({
    input: path,
    output,
    loudness: {
      normalization: 'ebuR128',
      target:
      {
        input_i: -23,
        input_lra: 7.0,
        input_tp: -2.0
      }
    }
  }).catch((error: any) => {
    // Some error happened
    console.error(error)
  })

  console.log(normalized)
  const finalOutputPath = `${outputDir}/${getName(output)}-prepared.wav`;
  return new Promise<string>(
    resolveFx => {
      ffmpeg(output, { logger: console })
        .audioFrequency(16000) // required for whisper
        .outputOptions(['-ac 1', '-c:a pcm_s16le'])
        .save(finalOutputPath)
        .on('end', () => {
          resolveFx(finalOutputPath)
        })
    })
}


export default class Prep extends Command {
  static description = 'Prepare audio file - normalize, noise reduce, split on silence'

  static DEFAULTS = {
    SILDUR: '1.3',
    SILBUF: 0.2,
    SILTHR: '-35dB',
    RNNN: 'https://raw.githubusercontent.com/GregorR/rnnoise-models/master/somnolent-hogwash-2018-09-01/sh.rnnn'
  }

  static args = { input: Args.string({ char: 'i', description: 'input file', required: true }) }
  static flags = {
    norm: Flags.boolean({ char: 'n', description: 'do normalization?', required: false, default: false }),
    verbose: Flags.boolean({ char: 'v', description: 'trace more?', required: false, default: false }),
    outdir: Flags.string({ char: 'o', description: 'output directory', required: false, default: './out' }),
    silthr: Flags.string({ char: 's', description: 'silence threshold', required: false, default: Prep.DEFAULTS.SILTHR }),
    sildur: Flags.string({ char: 'd', description: 'silence duration', required: false, default: Prep.DEFAULTS.SILDUR }),
  }

  /**
   * subroutines
   */
  static normalize = doNormalization
  static removeSilence = doSilenceRemoval

  /**
   * cli only (depends on config and fs situation)
   */
  static ensureRnnnIsCached = async (config: Config) => {
    const cachedRnnnPath = `${config.cacheDir}/sh.rnnn`;
    const isRnnnExisting = fs.existsSync(cachedRnnnPath)
    if (!isRnnnExisting) {
      console.log(`
      rnnn for noise removal missing
      Fetching ${Prep.DEFAULTS.RNNN} into ${cachedRnnnPath}
    `)
      const data = await fetch(Prep.DEFAULTS.RNNN)
      fs.writeFileSync(cachedRnnnPath, await data.text(), /* callback will go here */);
    } else console.log(`Found ${cachedRnnnPath} \n`)
    return cachedRnnnPath
  }

  /**
   * doPrep
   */
  public async doPrep(props: PrepProps) {
    const { input, outdir: outdirFlag, silthr, sildur, norm, verbose } = props
    // HACK ESM imports and geScrewed in oclif
    const { DEBUG, ERROR } = { DEBUG: console.log, ERROR: (...Any: any[]) => new Error(JSON.stringify(Any)) }//Logger.setup(verbose ? Logger.DEBUG : Logger.INFO) // eslint-disable-line no-unused-vars
    DEBUG('Starting Prep with:', props)

    return new Promise<PrepResult>(async (resolveFx, rejectFx) => {
      const silenceInfo: SilInfo[] = []
      const info: GenericObject = { traces: [], silenceInfo }

      const sourceFileName = getNameWithExt(input)
      const cachedRnnnPath = await Prep.ensureRnnnIsCached(this.config)
      // const isCreateInterimArtifacts = true // ? consider implement chaining if this is false and add param

      const phase1input = input
      const phase1Name = getName(phase1input)
      const outDir = `${outdirFlag}/${phase1Name}`
      fs.mkdirSync(path.normalize(outDir), { recursive: true })
      const phase1OutputPath = `${outDir}/${phase1Name}-denoised.wav`
      const ffmpegChain = ffmpeg(phase1input, { logger: console })
      ffmpegChain
        .audioFilters([
          { // (i) ref: https://onelinerhub.com/ffmpeg/how-to-reduce-background-audio-noise-using-arnndn
            filter: 'arnndn',
            options: { m: cachedRnnnPath, mix: 1 } // mix:-1 means keep only noise https://ffmpeg.org/ffmpeg-filters.html#arnndn
          },
          {
            filter: 'silencedetect',
            options: { n: silthr, d: sildur }
          },
        ])
        .audioFrequency(16000) // required for whisper
        .outputOptions(['-ac 1', '-c:a pcm_s16le'])
        .on('codecData', function (data: any) {
          const codecInfo = 'Input is ' + data.audio + ' audio ' + 'with ' + data.video + ' video';
          info.traces.push([codecInfo])
          DEBUG(codecInfo);
        })
        .on('stderr', function (stderrLine) {
          if (stderrLine.includes(' silence_end: ')) {
            const endInfo: string[] = stderrLine.split(' silence_end: ')[1].split(' | silence_duration: ')
            const [endSec, duration] = endInfo.map(s => +s)
            silenceInfo.push({ stSec: endSec - duration, endSec, duration })
          }
        })
        .on('error', function (error) {
          rejectFx(ERROR('Error:', error, silthr))
        })
        .on('end', async function (stdout, stderr) {
          const successTrace = ['Phase2 succeeded: ', { silenceInfo, phase2OutputPath: phase1OutputPath }]
          info.traces.push(successTrace); DEBUG(successTrace)
          ux.table(silenceInfo, { stSec: {}, endSec: {}, duration: {} })

          const outputPath = norm ? await Prep.normalize(phase1OutputPath, outDir) : phase1OutputPath

          const results = { ffmpegChain, outputPath }
          resolveFx({ props, info, results })
        })
        .save(phase1OutputPath)

    })
  }
  /**
   * only for cli parsing
   */
  async run(): Promise<void> {
    const { args: { input }, flags, flags: { outdir, silthr, sildur, norm, verbose } } = await this.parse(Prep)
    const report = await this.doPrep({ input, outdir, silthr, sildur, norm, verbose })
    verbose && console.log(report)
  }



  static examples = [
    `$ tranz prep AI_Could_Be_The_End_Of_Democracy.m4a' -s -35dB -d 1.5
--> sets silence threshold to -35dB and min silence duration to 1.5s
--> runs normalization and noise removal
`,
  ]
}
