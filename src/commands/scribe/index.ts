import { Args, Command, Flags } from '@oclif/core'
import { spawn } from 'child_process'
import * as fs from 'fs'
import { pipeline } from 'node:stream'
import { promisify } from 'node:util'
import path from 'path'
import { PipelineSource } from 'stream'
import { getName } from '../../utils/file-utils.js'
import Prep from '../prep/index.js'
type WhisperJsonOutput = Record<string, any>
type TranscriptionResult = { code: number | null, trace: string, trans: WhisperJsonOutput }

const fromToStr = {
  "from": "00:00:00,000",
  "to": "00:00:05,820"
}

const fromToNum = {
  "from": 0,
  "to": 5820
}
const transcriptionEntryEx = {
  "timestamps": fromToStr,
  "offsets": fromToNum,
  "text": " Learning from successes and failures is key to any type of experimentation.",
  "speaker_turn_next": false
}
export type FromToNum = typeof fromToNum
export type FromToStr = typeof fromToStr
export type TranscriptionEntry = typeof transcriptionEntryEx
export type TranscriptionArray = TranscriptionEntry[]
export type TranscribeParams = {
  inputPath: string,
  outputDir: string,
  modelPath?: string,
  diarize?: boolean,
  modelKey?: keyof typeof Scribe.DEFAULTS.MODEL_KEYS,
}

export default class Scribe extends Command {
  static DEFAULTS = {
    DIARIZE: false,
    SILDUR: '1.3',
    SILBUF: 0.2,
    SILTHR: '-35dB',
    MODEL_KEYS: {
      tinyd: 'ggml-small.en-tdrz.bin',
      small: 'ggml-small.bin',
      medium: 'ggml-medium.bin',
    },
    MODELS: {
      tinyd: 'https://huggingface.co/akashmjn/tinydiarize-whisper.cpp/resolve/main/ggml-small.en-tdrz.bin',
      small: 'https://huggingface.co/ggerganov/whisper.cpp/resolve/main/ggml-small.bin',
      medium: 'https://huggingface.co/ggerganov/whisper.cpp/resolve/main/ggml-medium.bin',
    }
  }

  static description = 'Transcribe audio file - optionally prepare first'

  static examples = [
    `$ tranz scribe 'path/to/16khz-audiofile.wav'
runs whisper and outputs and saves a transcription json

$ tranz scribe 'path/to/whatever-audiofile.mp3' -p
first prepares and then runs whisper and outputs and saves a transcription json
`,
  ]

  static flags = {

    prep: Flags.boolean({ char: 'p', description: 'do prep?', required: false, default: false }),
    norm: Flags.boolean({ char: 'n', description: 'do normalization?', required: false, default: false }),
    output: Flags.string({ char: 'o', description: 'output directory', required: false, default: './out' }),
    separate_speakers: Flags.boolean({ char: 'd', description: 'separate via diarization', required: false, default: Scribe.DEFAULTS.DIARIZE }),
  }

  static args = {
    input: Args.string({ char: 'i', description: 'input file', required: true }),
  }

  public transcribe = async ({
    inputPath,
    outputDir,
    modelPath,
    diarize,
    modelKey,
  }: TranscribeParams) => {
    modelKey = modelKey ?? diarize ? 'tinyd' : 'small' // irrelevant if modelPath is passed
    modelPath = modelPath ?? await this.ensureRequestedModelIsCached(modelKey)
    const sourceFileName = getName(inputPath)
    const model = `-m '${modelPath}'`



    const outTransPath = `${outputDir}/${sourceFileName}-transcript`
    const outTrans = `-of ${outTransPath}`
    const tdrz = diarize ? '-tdrz' : ''
    const options = `${tdrz} -t 8 -oj -f '${inputPath}' ${model} ${outTrans}`
    const cmd = `whisper-cpp ${options}`
    console.log('spawning  ', cmd)
    const whisperThread = spawn(`whisper-cpp`, options.split(' '), { shell: true })

    return new Promise<TranscriptionResult>(
      resolveFx => {
        let whisperOutput = ''
        const handleOut = (data: string) => {
          const str = data.toString();
          for (const match of ['[', 'main:']) {
            if (str.startsWith(match) || str.includes('total time')) console.log(str)
          }

          whisperOutput += data
        }
        whisperThread.stdout.on('data', handleOut)
        whisperThread.stderr.on('data', handleOut)

        whisperThread.on("close", (code) => {
          const trans = JSON.parse(fs.readFileSync(`${outTransPath}.json`).toString())
          resolveFx({ code, trace: whisperOutput, trans } as TranscriptionResult);
        })
        whisperThread.on('error', (err) => {
          console.error('Whisper Error', { err, outTransPath, options });
        })
      },
      // rejectFx => ??
    ).catch((whisperError) => console.error('Somehow uncaught Whisper Error', whisperError))
    /* example command ****
     whisper-cpp
     -f ../../Samples/FutureMatrixPart3_original_normalized-16k.wav
     -m ../../models/ggml-small.en-tdrz.bin
     -of '../../Samples/FutureMatrixPart3_original_normalized-16k'
     -tdrz  -t 12 -oj
     ****/
  }

  /**
     * cli only (depends on config and fs situation)
     */
  public ensureRequestedModelIsCached = async (modelKey: keyof typeof Scribe.DEFAULTS.MODEL_KEYS) => {
    if (!Scribe.DEFAULTS.MODEL_KEYS[modelKey]) throw new Error(`${modelKey} not known`)
    const cachedModelsDirPath = `${this.config.cacheDir}/models`
    fs.existsSync(cachedModelsDirPath) || fs.mkdirSync(cachedModelsDirPath, { recursive: true })
    const modelPath = `${cachedModelsDirPath}/${Scribe.DEFAULTS.MODEL_KEYS[modelKey]}`
    const isModelExisting = fs.existsSync(modelPath)
    if (!isModelExisting) {
      const srcURL = Scribe.DEFAULTS.MODELS[modelKey];
      this.log(`
    requested model is missing
    Fetching ${srcURL} into ${modelPath}
  `)
      const data = await fetch(srcURL)
      if (!data?.body) throw new Error('fetch failed')
      const streamPipeline = promisify(pipeline)
      await streamPipeline(data.body as unknown as PipelineSource<any>, fs.createWriteStream(modelPath))
    } else this.log(`Found ${modelPath} \n`)
    return modelPath
  }

  async run(): Promise<void> {
    const { args: { input }, flags, flags: { output: outputFlag, separate_speakers, prep, norm } } = await this.parse(Scribe)
    let inputPath = input
    const sourceFileName = getName(inputPath)
    if (prep) {
      const prepCommand = new Prep([], this.config)
      const prepResult = prepCommand.doPrep({ input: inputPath, outdir: outputFlag, norm, verbose: true, silthr: '35dB', sildur: '1.2' })
      inputPath = (await prepResult).results.outputPath
    }
    const outputDir = `${outputFlag}/${sourceFileName}`
    fs.mkdirSync(path.normalize(outputDir), { recursive: true })
    this.log(`\n   Transcribing ${inputPath} into ${outputDir}, maybe \n`)
    const transResults = await this.transcribe({ inputPath, outputDir, diarize: separate_speakers })
    if (!transResults) throw new Error(`bum trans ${JSON.stringify(flags, null, 2)}`)
    const { code, trace, trans } = transResults
    console.log('Transcription results: ', { code, trace, trans })
  }
}
