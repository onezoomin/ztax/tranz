import { Args, Command, Flags } from '@oclif/core'
// import { Logger } from '@ztax/logger/src'
import { getName } from '../../utils/file-utils.js'

const splitOnSil = (silenceInfo: any[], output: string, sourceFileName: string) => {
  const splitSeeks = []
  let report = `
        Exclusions and inclusions:
        `
  let segmentDuration = 0
  for (const [inx, eachSil] of silenceInfo.entries()) {
    const { stSec, endSec, duration } = eachSil
    segmentDuration = duration
    const nextSil = silenceInfo[inx + 1]

    if (!nextSil) break

    const seekExclude = (duration - (Split.DEFAULTS.SILBUF * 2)).toFixed(3)
    const segmentSt = +(endSec - Split.DEFAULTS.SILBUF).toFixed(3)
    const segmentDur = +((nextSil?.stSec ?? 0) + Split.DEFAULTS.SILBUF - segmentSt).toFixed(3)

    report += ` ex:${seekExclude} - in:${segmentDur}`

    const ts = 'todo'
    const outputPath = `${output}/${inx}-${ts}-from-${sourceFileName}.wav` as string
    splitSeeks.push([segmentSt, segmentDur, ts, outputPath])
  }
  report += ` ex:${segmentDuration.toFixed(3)} `
}
const doSplit = () => {
  const { WARN, LOG, DEBUG, VERBOSE, ERROR } = { WARN: console.warn, LOG: console.log, DEBUG: console.log, VERBOSE: console.debug, ERROR: (...Any: any[]) => new Error(JSON.stringify(Any)) }// Logger.setup(Logger.INFO); // eslint-disable-line no-unused-vars
  LOG('//TODO')
  // return new Promise<SplitResult>(async (resolveFx, rejectFx) => {

  //   const transResults = await getWhisperTranscript(phase1OutputPath, output)



  // if (!transResults?.trans?.transcription) return rejectFx(ERROR('bum transcription', transResults))
  // const { code, trace, trans } = transResults
  // const transTrace = ['Transcription results: ', { code, trace, trans }]
  // info.traces.push(transTrace); DEBUG(transTrace)

  // const transcription = trans.transcription as TranscriptionArray


  // const isCreateInterimArtifacts = true // TODO implement chaining if this is false and add param

  // const splitSeeks = []
  // for (const [inx, eachTrans] of transcription.entries()) {
  //   const { offsets: { from: segSt, to: segEnd },
  //     timestamps: { from: tsFrom, to: tsTo },
  //     text, speaker_turn_next: isNewSpeaker } = eachTrans

  //   const {
  //     offsets: { from: nxSegSt, to: nxSegEnd },
  //     // timestamps: { from: tsFromNx, to: tsToNx },
  //     text: textNx, speaker_turn_next: isNewSpeakerNx } = transcription[inx + 1] ?? { offsets: {} }

  //   const gap = (nxSegSt ?? segEnd) - segEnd
  //   const duration = (segEnd + gap * 0.5) - segSt


  //   const ts = tsFrom // TODO absolute uts ts if startTimestamp is given or
  //   const outputPath = `${output}/${sourceFileName}-${inx}-${ts}-${text.substring(0, 42)}.wav` as string
  //   splitSeeks.push([segSt, duration, ts, outputPath])
  // }

  // console.log('starting segment splitting' /*, { splitSeeks,  leadingSil, leadingSilEnd, inputParams  }*/);


  // for (const [inx, [sSt, sDur, ts, outputPath]] of splitSeeks.entries()) {
  //   console.log({ inx, sSt, sDur, ts })

  //   ffmpeg(phase2OutputPath, { logger: console }) //.inputOptions(inputParams)
  //     .seekInput(sSt)
  //     .duration(sDur)
  //     .save(outputPath as string)
  //     .on('end', function (stdout, stderr) {



  //     })
  // }
  // console.log('wrote', splitSeeks.length, 'files to', output)
  // })
}
export default class Split extends Command {
  static description = 'Split audio file -  on silence, or split info from transcription'

  static DEFAULTS = {
    OUTDIR: './out',
    SILDUR: '1.3',
    SILBUF: 0.2,
    SILTHR: '-35dB',
  }

  static flags = {
    output: Flags.string({ char: 'o', description: 'output directory', required: false, default: Split.DEFAULTS.OUTDIR }),
    silthr: Flags.string({ char: 's', description: 'silence threshold', required: false, default: Split.DEFAULTS.SILTHR }),
    sildur: Flags.string({ char: 'd', description: 'silence duration', required: false, default: Split.DEFAULTS.SILDUR }),
  }

  static args = {
    input: Args.string({ char: 'i', description: 'input file', required: true }),
  }

  static split = doSplit

  async run(): Promise<void> {
    const { args: { input }, flags, flags: { output, silthr, sildur } } = await this.parse(Split)
    const sourceFileName = getName(input)
    this.log(`\n   Preparing ${flags.input} into ${output}, maybe \n`)

    const report = await Split.split()
    console.log(report)

  }


  static examples = [
    `$ tranz prep split /pathTo/audio.wav
...TODO
`,
  ]
}
