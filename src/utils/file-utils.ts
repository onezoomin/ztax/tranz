
import path from 'path'

export const getExt = (filePath: string) => {
  return getFileInfo(filePath).ext
}
export const getName = (filePath: string) => {
  return getFileInfo(filePath).name
}
export const getNameWithExt = (filePath: string) => {
  const { name, ext } = getFileInfo(filePath)
  return `${name}.${ext}`
}
export const getFileInfo = (filePath: string) => {
  const normed = path.normalize(filePath)
  return path.parse(normed)
}
